# windego-code-tracker

用于向 vue 项目编译后的 dom 上注入其对应编译前的代码在编辑器中位置信息的 webpack loader&plugin

## 安装

### 1. 安装 `vnode-loader` 和 `vnode-plugin`

在项目根目录执行以下命令：

```
yarn add windego-code-tracker -D
```

or

```
npm install windego-code-tracker -D
```

### 2. 修改 `vue.config.js` 文件

在 `vue.config.js` 文件中，添加如下的 chainWebpack 配置<b>（注意需要判定一下环境，该功能只用于开发环境下）</b>：

```js
// vue.config.js
module.exports = {
  chainWebpack: (config) => {
    // 添加如下代码，注意判别环境
    if (process.env.NODE_ENV === 'development') {
      const { CodeTrackerPlugin } = require('windego-code-tracker')
      config.module
        .rule('vue')
        .test(/\.vue$/)
        .use('code-tracker-loader')
        .loader('code-tracker-loader')
        .end()
      config.plugin('code-tracker-plugin').use(new CodeTrackerPlugin())
    }
  },
}
```
