const path = require('path')
const fs = require('fs')


function injectCode(port) {
  const filePath = path.resolve(__dirname, './element.html')
  let code = fs.readFileSync(filePath, 'utf-8')
  code = code.replace(/__PORT__/g, port)
  return code
};

module.exports = injectCode