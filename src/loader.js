const { parse } = require('@vue/compiler-sfc')

const InjectLineName = '_tc-row' // 注入的行名称
const InjectColumnName = '_tc-col' // 注入的列名称
const InjectPathName = '_tc-path' // 注入的路径名称
const InjectNodeName = '_tc-node' // 注入的节点名称

function getInjectContent(ast, source, filePath) {
  // type为1是为标签节点
  if (ast?.type === 1) {
    // 递归处理子节点
    if (ast.children && ast.children.length) {
      // 从最后一个子节点开始处理，防止同一行多节点影响前面节点的代码位置
      for (let i = ast.children.length - 1; i >= 0; i--) {
        const node = ast.children[i]
        source = getInjectContent(node, source, filePath)
      }
    }
    const codeLines = source.split('\n') // 把行以\n划分方便注入
    const line = ast.loc.start.line // 当前节点起始行
    const column = ast.loc.start.column // 当前节点起始列
    const columnToInject = column + ast.tag.length // 要注入信息的列(标签名后空一格)
    const targetLine = codeLines[line - 1] // 要注入信息的行
    const nodeName = ast.tag
    const newLine =
      targetLine.slice(0, columnToInject) +
      ` ${InjectLineName}="${line}" ${InjectColumnName}="${column}" ${InjectPathName}="${filePath}" ${InjectNodeName}="${nodeName}"` +
      targetLine.slice(columnToInject)
    codeLines[line - 1] = newLine // 替换注入后的内容
    source = codeLines.join('\n')
  }
  return source
}

/**
 * 
 * @param {*} content 
 * @returns 
 * @description 在编译vue文件时,在各个元素中注入代码的文件路径,代码行列等信息
 */
function CodeTrackerLoader(content) {
  const filePath = this.resourcePath // 当前文件的绝对路径
  let params = new URLSearchParams(this.resource)
  if (params.get('type') === 'template') {
    const vueParserContent = parse(content) // vue文件parse后的内容
    const domAst = vueParserContent?.descriptor?.template?.ast // template开始的dom ast结构
    if (!domAst) return content
    const templateSource = domAst.loc.source // template部分的原字符串
    const newTemplateSource = getInjectContent(domAst, templateSource, filePath) // 注入后的template部分字符串
    const newContent = content.replace(templateSource, newTemplateSource)
    return newContent
  } else {
    return content
  }
}

module.exports = CodeTrackerLoader
