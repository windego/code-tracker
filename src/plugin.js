const startServer = require('./server')
const injectCode = require('./injectCode')
class CodeTrackerPlugin {
  apply(complier) {
    complier.hooks.compilation.tap('CodeTrackerPlugin', (compilation) => {
      startServer((port) => {
        const code = injectCode(port)
        compilation.hooks.htmlWebpackPluginAfterHtmlProcessing.tap('HtmlWebpackPlugin', (data) => {
          // html-webpack-plugin编译后的内容，注入代码
          data.html = data.html.replace('</body>', `${code}\n</body>`)
        })
      })
    })
  }
}
module.exports = CodeTrackerPlugin
